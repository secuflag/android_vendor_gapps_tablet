# app
PRODUCT_PACKAGES += \
    com.android.chrome \
    com.google.android.syncadapters.calendar \
    com.google.android.syncadapters.contacts \
    com.google.android.inputmethod.latin

# etc
PRODUCT_COPY_FILES += \
    vendor/gapps/tablet/system/etc/default-permissions/default-permissions.xml:system/etc/default-permissions/default-permissions.xml \
    vendor/gapps/tablet/system/etc/default-permissions/opengapps-permissions.xml:system/etc/default-permissions/opengapps-permissions.xml \
    vendor/gapps/tablet/system/etc/permissions/com.google.android.maps.xml:system/etc/permissions/com.google.android.maps.xml \
    vendor/gapps/tablet/system/etc/permissions/com.google.android.media.effects.xml:system/etc/permissions/com.google.android.media.effects.xml \
    vendor/gapps/tablet/system/etc/permissions/privapp-permissions-google.xml:system/etc/permissions/privapp-permissions-google.xml \
    vendor/gapps/tablet/system/etc/sysconfig/google.xml:system/etc/sysconfig/google.xml \
    vendor/gapps/tablet/system/etc/sysconfig/google_build.xml:system/etc/sysconfig/google_build.xml \
    vendor/gapps/tablet/system/etc/sysconfig/google_exclusives_enable.xml:system/etc/sysconfig/google_exclusives_enable.xml \
    vendor/gapps/tablet/system/etc/sysconfig/google-hiddenapi-package-whitelist.xml:system/etc/sysconfig/google-hiddenapi-package-whitelist.xml

# framework
PRODUCT_PACKAGES += \
    com.google.android.maps \
    com.google.android.media.effects

# priv-app
PRODUCT_PACKAGES += \
    com.google.android.configupdater \
    com.google.android.gms.setup \
    com.google.android.backuptransport \
    com.google.android.onetimeinitializer \
    com.google.android.apps.restore \
    com.google.android.gsf \
    com.android.vending \
    com.google.android.gms
